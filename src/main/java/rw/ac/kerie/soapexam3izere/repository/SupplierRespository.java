package rw.ac.kerie.soapexam3izere.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rw.ac.kerie.soapexam3izere.bean.Supplier;

@Repository
public interface SupplierRespository extends JpaRepository<Supplier,Integer> {
}
