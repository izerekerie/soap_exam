package rw.ac.kerie.soapexam3izere.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.kerie.soapexam3izere.bean.Item;
import rw.ac.kerie.soapexam3izere.bean.Supplier;
import rw.ac.kerie.soapexam3izere.classes.items.*;
import rw.ac.kerie.soapexam3izere.classes.suppliers.*;
import rw.ac.kerie.soapexam3izere.repository.ItemRepository;
import rw.ac.kerie.soapexam3izere.repository.SupplierRespository;


import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndpoint {
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    SupplierRespository supplierRespository;
    //    create item


    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/items", localPart = "InsertItemRequest")
    @ResponsePayload
//    create supplier
    public InsertItemResponse insertItem(@RequestPayload InsertItemRequest request) {
        Item item= mapItemDetails(request.getItemDetails());
        itemRepository.save(item);
        InsertItemResponse response = new InsertItemResponse();
        response.setItemDetails(mapItem(item));
        return  response;
    }
    //get all item

    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/items", localPart = "GetAllItemsRequest")
    @ResponsePayload
    public GetAllItemsResponse getAll(@RequestPayload GetAllItemsRequest request) {

        GetAllItemsResponse allItemsDetails = new GetAllItemsResponse();
        List<Item> items = itemRepository.findAll();
        for (Item item : items) {
            ItemDetails itemDetails = mapItem(item);
            allItemsDetails.getItemDetails().add(itemDetails);

        }

        return allItemsDetails;
    }


    // update item

    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/items", localPart = "UpdateItemRequest")
    @ResponsePayload
    public UpdateItemResponse updateItem(@RequestPayload UpdateItemRequest request) {
        Item item = new Item();

        ItemDetails itemDetails = request.getItemDetails();
        item = itemRepository.findById(itemDetails.getId()).get();
        if (item == null) {
            return new UpdateItemResponse();
        }
        UpdateItemResponse response = new UpdateItemResponse();
        response.setItemDetails(mapItem(item));

        itemRepository.save(item);
        return  response;


    }
    // delete
    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/items", localPart = "DeleteItemRequest")
    @ResponsePayload
    public DeleteItemResponse deleteById(@RequestPayload DeleteItemRequest request) {

        Optional<Item> item=itemRepository.findById(request.getId());
        if(item.isPresent()){
            supplierRespository.deleteById(request.getId());
            return new DeleteItemResponse();
        }

        return  null;
    }
    // get one item
    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/items", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse getById(@RequestPayload GetItemDetailsRequest request) {
        Item item = itemRepository.findById(request.getId()).get();
        if (item == null) {
            return new GetItemDetailsResponse();
        }
        GetItemDetailsResponse response = new GetItemDetailsResponse();
        response.setItemDetails(mapItem(item));
        return response;


    }

    private Supplier mapSupplDetails(SupplierDetails suppD){
        Supplier supplier=new Supplier();
        supplier.setId(suppD.getId());
        supplier.setEmail(suppD.getEmail());
        supplier.setMobile(suppD.getMobile());
        supplier.setNames(suppD.getNames());
        return  supplier;
    }
    private Item mapItemDetails(ItemDetails itemD){
        Item item=new Item();

        item.setId(itemD.getId());
        item.setItemCode(itemD.getItemCode());
        item.setName(itemD.getName());
        item.setPrice(itemD.getPrice());
        item.setStatus(itemD.getStatus());
        item.setSupplier(supplierRespository.findById(itemD.getSupplier()).get());
        return  item;
    }
    private ItemDetails mapItem(Item item){
        ItemDetails itemD=new ItemDetails();
        itemD.setId(item.getId());
        itemD.setItemCode(item.getItemCode());
        itemD.setName(item.getName());
        itemD.setPrice(item.getPrice());
        itemD.setStatus(item.getStatus().toString());


        itemD.setSupplier(item.getSupplier().getId());
        return  itemD;
    }
}
