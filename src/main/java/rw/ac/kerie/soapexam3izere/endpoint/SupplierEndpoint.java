package rw.ac.kerie.soapexam3izere.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.kerie.soapexam3izere.bean.Supplier;
import rw.ac.kerie.soapexam3izere.classes.suppliers.*;
import rw.ac.kerie.soapexam3izere.repository.SupplierRespository;


import java.util.List;
import java.util.Optional;

@Endpoint
public class SupplierEndpoint {

    @Autowired
    SupplierRespository supplierRespository;

    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/suppliers", localPart = "CreateSupplierRequest")
    @ResponsePayload
//    create supplier
    public CreateSupplierResponse insertSupplier(@RequestPayload CreateSupplierRequest request) {
        Supplier supplier= mapSupplDetails(request.getSupplierDetails());
        supplierRespository.save(supplier);
        CreateSupplierResponse response=new CreateSupplierResponse();
        response.setSupplierDetails(mapSupplier(supplier));
        return  response;
    }
    //get all supplier

    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/suppliers", localPart = "GetAllSupplierRequest")
    @ResponsePayload
    public GetAllSupplierResponse getAll(@RequestPayload GetAllSupplierRequest request) {

        GetAllSupplierResponse allSuppliersDetails = new GetAllSupplierResponse();
        List<Supplier> suppliers = supplierRespository.findAll();
        for (Supplier course : suppliers) {
            SupplierDetails courseDetails = mapSupplier(course);
            allSuppliersDetails.getSupplierDetails().add(courseDetails);

        }

        return allSuppliersDetails;
    }

    // get one supplier
    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/suppliers", localPart = "GetOneSupplierRequest")
    @ResponsePayload
    public GetOneSupplierResponse getById(@RequestPayload GetOneSupplierRequest request) {
        Supplier supplier = supplierRespository.findById(request.getId()).get();
        if (supplier == null) {
            return new GetOneSupplierResponse();
        }
        GetOneSupplierResponse response = new GetOneSupplierResponse();
        response.setSupplierDetails(mapSupplier(supplier));
        return response;


    }
// delete supplier


    // delete
    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/suppliers", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse deleteById(@RequestPayload DeleteSupplierRequest request) {

        Optional<Supplier> supplier=supplierRespository.findById(request.getId());
        if(supplier.isPresent()){
            supplierRespository.deleteById(request.getId());
            return new DeleteSupplierResponse();
        }

        return  null;
    }
    // update supplier

    @PayloadRoot(namespace = "http://rw/ac/kerie/soapexam3izere/classes/suppliers", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse updateCourse(@RequestPayload UpdateSupplierRequest request) {
        Supplier supplier = new Supplier();

        SupplierDetails supplierDetails = request.getSupplierDetails();

        supplier = supplierRespository.findById(supplierDetails.getId()).get();
        if (supplier == null) {
            return new UpdateSupplierResponse();
        }
        supplier=mapSupplDetails(supplierDetails);


        supplierRespository.save(supplier);
        UpdateSupplierResponse supplierResponse = new UpdateSupplierResponse();
        supplierResponse.setSupplierDetails(mapSupplier(supplier));
        return supplierResponse;
    }

    private Supplier mapSupplDetails(SupplierDetails suppD){
        Supplier supplier=new Supplier();
        supplier.setId(suppD.getId());
        supplier.setEmail(suppD.getEmail());
        supplier.setMobile(suppD.getMobile());
        supplier.setNames(suppD.getNames());
        return  supplier;
    }
    private SupplierDetails mapSupplier(Supplier supplier){
        SupplierDetails supplierD=new SupplierDetails();
        supplierD.setId(supplier.getId());
        supplierD.setEmail(supplier.getEmail());
        supplierD.setMobile(supplier.getMobile());
        supplierD.setNames(supplier.getNames());
        return  supplierD;
    }
}
