package rw.ac.kerie.soapexam3izere.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

//Enable Spring Web Services
@EnableWs
//Spring Configuration
@Configuration
public class WebServiceConfig {

    // MessageDispatcherServlet
    // ApplicationContext
    // url -> /ws/*

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/kerie/*");
    }

    // /ws/kerie/items.wsdl

    @Bean(name = "items")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema itemsSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemPort");
        definition.setTargetNamespace("http://rw/ac/kerie/soapexam3izere/classes/items");
        definition.setLocationUri("/ws/kerie");
        definition.setSchema(itemsSchema);
        return definition;
    }

    @Bean
    public XsdSchema itemsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("items-xsd.xsd"));
    }

    @Bean(name = "suppliers")
    public DefaultWsdl11Definition Wsdl11Definition(XsdSchema suppliersSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupplierPort");
        definition.setTargetNamespace("http://rw/ac/kerie/soapexam3izere/classes/suppliers");
        definition.setLocationUri("/ws/kerie");
        definition.setSchema(suppliersSchema);
        return definition;
    }

    @Bean
    public XsdSchema suppliersSchema() {

        return new SimpleXsdSchema(new ClassPathResource("suppliers-xsd.xsd"));
    }
}