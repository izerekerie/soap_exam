package rw.ac.kerie.soapexam3izere.bean;

import rw.ac.kerie.soapexam3izere.enums.ItemStatus;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Item {
    @Id
    private int id;
    private String name;
    private String itemCode;
    private int price;
    private ItemStatus status;

    @OneToOne
    private Supplier supplier;

    public int getId() {
        return id;
    }

    public Item() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = ItemStatus.valueOf(status);
    }

    public Supplier getSupplier() {
        return this.supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Item(int id, String name, String itemCode, int price, ItemStatus status, Supplier supplier) {
        this.id = id;
        this.name = name;
        this.itemCode = itemCode;
        this.price = price;
        this.status = status;
        this.supplier = supplier;
    }
}
