package rw.ac.kerie.soapexam3izere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Soapexam3izereApplication {

    public static void main(String[] args) {
        SpringApplication.run(Soapexam3izereApplication.class, args);
    }

}
